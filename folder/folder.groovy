folder('ANNASIK-APPS') {
    description('This is the folder for the ANNASIK-APPS')
}

folder('ANNASIK-APPS/dev') {
    description('This is the folder for the dev jobs')
}

folder('ANNASIK-APPS/staging') {
    description('This is the folder for the staging jobs')
}

folder('ANNASIK-APPS/prod') {
    description('This is the folder for the prod jobs')
}

folder('ANNASIK-APPS/dev/ANNASIK-BACKEND') {
    description('This is the folder for the backend dev jobs')
}

folder('ANNASIK-APPS/staging/ANNASIK-BACKEND') {
    description('This is the folder for the backend staging jobs')
}

folder('ANNASIK-APPS/prod/ANNASIK-BACKEND') {
    description('This is the folder for the backend prod jobs')
}


folder('ANNASIK-APPS/dev/ANNASIK-FRONTEND') {
    description('This is the folder for the frontend dev jobs')
}

folder('ANNASIK-APPS/staging/ANNASIK-FRONTEND') {
    description('This is the folder for the frontend staging jobs')
}

folder('ANNASIK-APPS/prod/ANNASIK-FRONTEND') {
    description('This is the folder for the frontend prod jobs')
}

