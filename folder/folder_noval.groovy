folder('NOVAL-APPS') {
    description('This is the folder for the NOVAL-APPS')
}

folder('NOVAL-APPS/dev') {
    description('This is the folder for the dev jobs')
}

folder('NOVAL-APPS/staging') {
    description('This is the folder for the staging jobs')
}

folder('NOVAL-APPS/prod') {
    description('This is the folder for the prod jobs')
}

folder('NOVAL-APPS/dev/BACKEND') {
    description('This is the folder for the backend dev jobs')
}

folder('NOVAL-APPS/staging/BACKEND') {
    description('This is the folder for the backend staging jobs')
}

folder('NOVAL-APPS/prod/BACKEND') {
    description('This is the folder for the backend prod jobs')
}


folder('NOVAL-APPS/dev/FRONTEND') {
    description('This is the folder for the frontend dev jobs')
}

folder('NOVAL-APPS/staging/FRONTEND') {
    description('This is the folder for the frontend staging jobs')
}

folder('NOVAL-APPS/prod/FRONTEND') {
    description('This is the folder for the frontend prod jobs')
}

