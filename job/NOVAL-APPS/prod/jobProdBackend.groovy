import groovy.json.JsonSlurperClassic

// Path where the Job DSL can access the file
def jsonFile = readFileFromWorkspace('config/NOVAL-APPS/prod/backendAppsConfiguration.json')

println "Config file found, processing..."
def configJson = new JsonSlurperClassic().parseText(jsonFile)

configJson.service.each { service ->
    service.project.each { project ->
        println "Processing project: ${project.name}"
        println "Group: ${project.group}, Environment: ${project.environment}, Type: ${project.serviceType}"

        def folderingName = "${project.group}/${project.environment}/${project.subGroup}"
        def jobName = "${project.environment}-${project.name}"
        def repoUrl = "https://gitlab.com/${project.group}/${project.subGroup}/${project.name}.git"
        def branch = project.branch

        println "Creating job: ${folderingName}/${jobName}"
        println "Repo URL: ${repoUrl}"
        println "Branch: ${branch}"

        pipelineJob(folderingName + "/" + jobName) {
            definition {
                cps {
                script(readFileFromWorkspace("pipeline/${project.serviceType}/${project.serviceLanguage}/Jenkinsfile"))
                sandbox()
                }
            }

            parameters {
                choiceParam('DEPLOYMENT_ACTION', ['','Rollback', 'Redeploy'], 'You can choose to rollback or redeploy the service 🔍')
                stringParam('DEPLOYMENT_VERSION', '', 'You must provide the tag version from the gitlab repository to rollback or redeploy the service with the format: X.X.X-RELEASE 🔑')
                stringParam('SERVICE_GROUP', project.group, '⛔️ Warning: Do not change this value ⛔️')
                stringParam('SERVICE_SUBGROUP', project.subGroup, '⛔️ Warning: Do not change this value ⛔️')
                stringParam('SERVICE_NAME', project.name, '⛔️ Warning: Do not change this value ⛔️')
                stringParam('SERVICE_TYPE', project.serviceType, '⛔️ Warning: Do not change this value ⛔️')
                stringParam('SERVICE_ENVIRONMENT', project.environment, '⛔️ Warning: Do not change this value ⛔️')
                stringParam('SERVICE_LANGUAGE', project.serviceLanguage, '⛔️ Warning: Do not change this value ⛔️')
                stringParam('SERVICE_DOCKERFILE', project.dockerfile, '⛔️ Warning: Do not change this value ⛔️')
                choiceParam('SERVICE_NAME_PARAMS', project.module, 'Choose the service to deploy')
            }

            triggers {
                genericTrigger {
                    genericVariables {
                        genericVariable {
                            key('GIT_BRANCH')
                            value("\$.ref")
                            expressionType('JSONPath')
                        }

                        genericVariable {
                            key('SERVICE_NAME')
                            value("\$.project.name")
                            expressionType('JSONPath')
                        }

                        genericVariable {
                            key('SERVICE_SUBGROUP')
                            value("\$.project.namespace")
                            expressionType('JSONPath')
                        }

                        genericVariable {
                            key('PUSH_STATUS')
                            value("\$.object_kind")
                            expressionType('JSONPath')
                        }
                    }
                    token("${project.token}")
                    printContributedVariables(false)
                    printPostContent(false)
                    silentResponse(false)
                    regexpFilterText("\$GIT_BRANCH-\$PUSH_STATUS")
                    regexpFilterExpression('^refs/tags/.*-RELEASE-tag_push\$')

                }
            }
        }
    }
}

